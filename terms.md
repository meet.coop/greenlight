Terms of use {#terms_of_use}
============

This document is a proposal based on the Terms of Use of CommonsCloud:
<https://www.commonscloud.coop/en/terms-of-use/> and need revision.

1. Who is in charge of The Online Meeting Cooperative {#who_is_in_charge_of_the_online_meeting_cooperative}
-----------------------------------------------------

The Online Meeting Cooperative is a collective initiative co-produced as
a Consortium initiated by femProcomuns SCCL cooperative with Tax Number
F67139550 and headquarters at Providencia 42, 08024 Barcelona,
​​Webarchitects Cooperative, with headquarters in Sheffield, United
Kingdom and Collective.tools with headquarters in Gothenburg, Sweden,
which collectively act as the owner, facilitator and administrator of
meet.coop domain and subdomains. For clarity, in this document the term
"Meet.coop" refers to the The Online Meeting Cooperative project.

The contact details of the Delegate for Data Protection are:
contact\@meet.coop

2. Definitions
--------------

### 2.1 User

User is anyone who connects with one of Meet.coop websites or systems.

### 2.2. Members

Members are people and organisations that are part of The Online Meeting
Cooperative. We have two types of memberships--Operational Members and
User Members. One may apply to each membership type as an individual or
as an organization. Please see the [Membership
page](https://wiki.meet.coop/wiki/Membership) for more details.

### 2.2. Services

The Online Meeting Cooperative offers online video conferencing
services.

We provide free services available to anyone via demo.meet.coop and paid
services to our Members. The Online Meeting Cooperative seeks to make it
easier for people to use free services to help them realise they can
regain control over their data.

### 2.3. Private and public information {#private_and_public_information}

Users can decide on the privacy of their information within each of
their services, they decide if they want to keep their data private,
share it with other people or make it public. In the latter case, the
information is considered "public information" and is not protected in
the same way as personal and private data.

### 2.5. Free software {#free_software}

Free software, libre software or free-as-in-freedom-software is software
that can be used, studied and modified by anyone without restrictions,
and can be copied and redistributed either in a modified version or
unmodified without restrictions, or with minimal restrictions to ensure
that future recipients will also have these rights.

### 2.6. Free licenses {#free_licenses}

Since the laws of intellectual and industrial property turn information
into exclusive property by default, the free software movement has built
standard legal agreements to facilitate the sharing of knowledge, which
are known as "Free licenses". You'll find the precise definition and
licenses that are considered free at <http://freedomdefined.org/>. Each
free software is governed by its own rules defined in its license.
<http://freedomdefined.org/>.

### 2.7. Rates

Rates refer to the price of services that a user contracts - please see
the links to the latest prices via our [home page](https://meet.coop)

### 2.8. Voluntary contributions {#voluntary_contributions}

Users are encouraged to make voluntary contributions, in the form of a
donation, to support the project.

### 2.9. Contribution to the share capital {#contribution_to_the_share_capital}

To become a member, a user reads and accepts the Statutes of the
custodian cooperative through which membership is realised and makes a
unique contribution to the capital of the cooperative. Under conditions
as specified in the bylaws of each custodian member cooperative, the
share capital maybe returned upon leaving the cooperative.

### 2.10. Storage space {#storage_space}

Members may have a limited storage space to save recordings of meetings
at one of the servers.

### 2.11. Support channels {#support_channels}

The Online Meeting Cooperative offers Support Channels to facilitate the
user experience, such as community support among users, technical
documentation - see [the forum](https://forum.meet.coop/) for more
details.

3. Membership
-------------

In order to join The Online Meeting Cooperative you must become a Member
- this also gives you a vote in the assembly and access to channels of
participation for members. Please see the [Membership
page](https://wiki.meet.coop/wiki/Membership) for more details and join
via [Open Collective](https://opencollective.com/meet-coop/)

4. Form of payment {#form_of_payment}
------------------

Social capital contributions, service fees and voluntary contributions
are in Euros unless otherwise specified and will be made via [Open
Collective](https://opencollective.com/meet-coop/) or BACS.

5. Intellectual property {#intellectual_property}
------------------------

The Intellectual Property Law states that the authors have exclusive
rights to their work by default. At The Online Meeting Cooperative we
want the contrary: all knowledge generated and published as public
information in the free services available to the entire user community
is, by default, considered to be published under free licenses -- if the
opposite is not explicitly stated. Specifically, we use the licenses
mentioned below.

`   In all works and contributions made public in the free services that do not have specific restrictions on its use or specific licenses, the following license applies to each type of work:`\
`   • software: Affero General Public License v3 or higher`\
`   • content: Creative Commons Recognition-ShareAlike 30-EN`\
`   • data: Open Domain Commons Public Domain Dedication License `[`https://opendatacommons.org/licenses/pddl/`](https://opendatacommons.org/licenses/pddl/)\
`   • industrial designs: CERN Open Hardware License`

Shared and public information requires recognition of authorship, in
such a way that other users can contribute to their improvement,
extension and maintenance and to facilitate the collective construction
of knowledge with respect for all contributions. However, logos, trade
names, trademarks or any other sign owned by third parties are excluded
from this license.

The user, when contracting or using with The Online Meeting Cooperative,
accepts the intellectual property policy of the project and therefore of
the service.

Any shared content that belongs to authors who are not users of the The
Online Meeting Cooperative project is protected by the intellectual
protection rules they have chosen, and are not subject to these
conditions. Users are responsible in this case to respect the rules of
intellectual property, and mainly the authorship and the license on the
basis of which they have the right to share it.

6.- Obligations and responsibilities of the User {#obligations_and_responsibilities_of_the_user}
------------------------------------------------

The User (Members of Meet.coop) agrees to communicate to The Online
Meeting Cooperative, when creating their account and membership their
exact contact information and to keep them up to date, so that The
Online Meeting Cooperative can keep direct communication with them.

The User is the only person responsible for the passwords they need for
the use of the services, as well as for their storage.

The User assumes in full the consequences in the event of the loss or
theft of their passwords.

The User is responsible for the contents published on the servers,
whether in free services or payed services, as well as of the
communications made through them. For this reason the User undertakes to
make a responsible use and assume all the responsibilities derived from
its publications.

7.- Limitation of responsibility for The Online Meeting Cooperative {#limitation_of_responsibility_for_the_online_meeting_cooperative}
-------------------------------------------------------------------

The Online Meeting Cooperative is free to limit the access to web pages
and content, products and/or services offered therein, as well as the
subsequent publication of the opinions, observations, images or comments
that users can send through e-mail.

The Online Meeting Cooperative is committed to carrying out operations
to maintain, update and improve infrastructures. In case these
operations involve a temporary interruption of the service, The Online
Meeting Cooperative is committed to let users know.

In no case may the User make a claim against The Online Meeting
Cooperative for any direct or indirect personal or commercial loss
derived from the interruption of the service.

The Online Meeting Cooperative has no responsibility for hardware damage
in any of the services offered.

In case of incidents occurring in a third-party data processing center,
The Online Meeting Cooperative will use all the guarantees offered by
the company that owns the data processing center in favor of the User.

In no case may the User claim responsibility for The Online Meeting
Cooperative in case of fire, explosion, failure of transmission
networks, demolition of facilities, epidemic, earthquake, flood,
electrical failure, war, strike, boycott or any other circumstance of
force majeure.

The Online Meeting Cooperative is not responsible for the damages caused
by the lack, neglect or omission of third parties on which The Online
Meeting Cooperative has no control or surveillance power.

It is the responsibility of the User to take all necessary measures to
safeguard the integrity of their data.

The Online Meeting Cooperative is not responsible for the total or
partial destruction of the information transmitted or stored as a result
of errors directly or indirectly attributable to the User or its
collaborators. In the case of failure of the service, for a failure of
the system caused by The Online Meeting Cooperative will not be
responsible of any indirect damages, such as commercial damage, loss of
orders, loss of profits or customers, they are expressly excluded.

The sum of the damages and interests that could be placed by The Online
Meeting Cooperative, should its responsibility be compromised, will be
limited to the effective sum paid by the User to The Online Meeting
Cooperative for the period considered or billed to the User by The
Online Meeting Cooperative.

8.- About these conditions {#about_these_conditions}
--------------------------

All notices, requirements, citations and other communications that must
be made by the parties in relation to these conditions must be made in
writing and it will be understood that they have been properly executed
when delivered in hand or either remitted by ordinary mail to the
address of the other party or to their email address or to any other
address or email that for these purposes each part may indicate to the
other.

If any clause included in these conditions is declared, totally or
partially, null or ineffective, such invalidity or inefficiency will
affect only this disposition or the part of it that is null or
ineffective, the same conditions remain for all the rest, considering
such a disposition, or the part of it that is affected, not estated.

The Online Meeting Cooperative will proceed to update these conditions
when it deems it is appropriate or if changes are made in the legal
framework. These will always occur after having notified the users.

0=9.- Jurisdiction and current law== This agreement will be governed and
interpreted in accordance with the laws applicable in (Catalonia, UK,
Sweden). The parties are subject to the common rules of the
jurisdiction.

Last update: July 2020
